FROM node:14
WORKDIR /usr/src/kube_test
COPY package*.json /usr/src/kube_test/
RUN npm i
COPY . /usr/src/kube_test/
EXPOSE 3000
CMD [ "npm", "start" ]
