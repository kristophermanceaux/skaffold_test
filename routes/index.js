var express = require('express');
var router = express.Router();
var { Client } = require('pg');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/submit-form', function (req, res, next) {
  const client = new Client({
    user: 'postgres',
    host: 'my-psql',
    database: 'kube_test',
    password: 'postgres',
    port: 5432
  });
  (async function () {
    await client.connect()
    const res = await client.query('insert into Test.Names (name) values ($1::text);', [req.body.name])
    await client.end()
  }());

  res.redirect('/');
});

module.exports = router;
