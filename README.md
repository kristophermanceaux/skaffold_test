# Skaffold test for local Kubernetes development

##  Steps on how you can test drive this app
- Download [Skaffold](https://skaffold.dev/)
- Clone the repository
- Run `skaffold dev --port-forward`
  - By default Skaffold does not port forward, so if we did not specify this option: `--port-forward` we would not be able to access the application on the exposed port 3000
- Watch Skaffold build all of your images and run the containers
- Go to `localhost:3000` in your browser
- Try making some changes and watch your images automatically rebuild and deploy upon saving

## What is this?
Out of a desire to migrate to a Kubernetes architecture for another project, but wanting a smoother development experience, I found Skaffold. This is just a test project where I created a simple Node.js project that uses a Postgres database, and wrapped them up in containers using Docker and Docker Compose. I then used Skaffold to convert my `docker-compose.yml` file into Kubernetes .yml files (which actually uses [Kompose](https://kompose.io/) under the hood) with this command:
```bash
skaffold init --compose-file docker-compose.yml \
 -a '{"builder": "Docker", "payload": {"path": "dockerfile"}, "image": "test_app"}' \
  -a '{"builder": "Docker", "payload": {"path": "database/dockerfile"}, "image": "my-psql"}'
```
This initializes Skaffold by creating all of the necessary kubernetes .yml files and also creates `skaffold.yml` which is specific to Skaffold.
Then all I really had to do was run `skaffold dev --port-forward`
